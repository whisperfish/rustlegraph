use std::path::Path;

use image::{Rgba, RgbaImage};
use rustlegraph::*;

fn main() -> anyhow::Result<()> {
    // Get the first command line argument.
    let args: Vec<String> = std::env::args().collect();
    let path = args.get(1).expect("file path not provided");

    let params = VizualizationParameters {
        width: 200,
        height: 80,
        past_color: Rgba([255, 255, 255, 255]),
        future_color: Rgba([128, 0, 0, 255]),
    };

    let viz = Vizualizer::from_file(params.clone(), None, Path::new(path))?;
    let mut img = RgbaImage::new(params.width, params.height);
    let time = Time {
        seconds: 2,
        frac: 0.,
    };
    viz.render_to_image(time, &mut img)?;
    img.save("output.png")?;

    Ok(())
}
