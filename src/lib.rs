use std::fs::File;
use std::path::Path;

use image::{ImageBuffer, Rgba};
use symphonia::core::audio::{AudioBufferRef, Signal};
use symphonia::core::codecs::{DecoderOptions, CODEC_TYPE_NULL};
use symphonia::core::errors::Error as SymphoniaError;
use symphonia::core::formats::{FormatOptions, FormatReader};
pub use symphonia::core::io::MediaSource;
use symphonia::core::io::MediaSourceStream;
use symphonia::core::meta::MetadataOptions;
use symphonia::core::probe::Hint;
pub use symphonia::core::units::Time;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Dimension supplied to render ({supplied_width}x{supplied_height}) does not equal the preprocessed dimensions ({preprocessed_width}x{preprocessed_height})")]
    DimensionMismatch {
        supplied_width: u32,
        supplied_height: u32,
        preprocessed_width: u32,
        preprocessed_height: u32,
    },
    #[error("Dimensions should be larger than 0")]
    ZeroDimensions,
    #[error("Could not find codec information")]
    CodecError(SymphoniaError),
    #[error("Error reading the audio")]
    AudioError(#[from] SymphoniaError),
    #[error("Unknown length")]
    UnknownLength,
    #[error("Audio data contains no samples")]
    EmptyAudioFragment,
    #[error("Error reading input")]
    IoError(#[from] std::io::Error),
}

pub struct Vizualizer {
    params: VizualizationParameters,
    waveform: Vec<u32>,
    peak: u32,
    time: Time,
}

#[derive(Clone, Debug)]
pub struct VizualizationParameters {
    pub width: u32,
    pub height: u32,

    pub past_color: Rgba<u8>,
    pub future_color: Rgba<u8>,
}

impl Vizualizer {
    pub fn from_file(
        params: VizualizationParameters,
        mime_type: Option<&str>,
        filename: &Path,
    ) -> Result<Self, Error> {
        let source: Box<dyn MediaSource> = Box::new(File::open(filename)?);
        Self::new(params, source, mime_type, Some(filename))
    }

    pub fn time(&self) -> &Time {
        &self.time
    }

    pub fn new(
        params: VizualizationParameters,
        source: Box<dyn MediaSource>,
        mime_type: Option<&str>,
        filename: Option<&Path>,
    ) -> Result<Self, Error> {
        let (waveform, time) = Self::extract_waveform(
            params.width as usize,
            source,
            mime_type,
            filename
                .and_then(Path::extension)
                .and_then(std::ffi::OsStr::to_str),
        )?;
        log::debug!("time: {:?} in {} samples", time, waveform.len());
        let peak = *waveform.iter().max().unwrap();
        Ok(Self {
            time,
            peak,
            params,
            waveform,
        })
    }

    pub fn render_to_image<Container>(
        &self,
        until: Time,
        img: &mut ImageBuffer<Rgba<u8>, Container>,
    ) -> Result<(), Error>
    where
        Container: std::ops::Deref<Target = [u8]> + std::ops::DerefMut,
    {
        if (img.width() != self.params.width) || (img.height() != self.params.height) {
            return Err(Error::DimensionMismatch {
                supplied_width: img.width(),
                supplied_height: img.height(),
                preprocessed_width: self.params.width,
                preprocessed_height: self.params.height,
            });
        }

        let until = until.seconds as f64 + until.frac;
        let time = self.time.seconds as f64 + self.time.frac;
        let until_x = self.waveform.len() as f64 * until / time;
        let until_x = until_x as usize;

        let baseline = self.params.height / 2;

        let peak = std::cmp::max(self.peak, 1);

        for x in 0..self.waveform.len() {
            // Draw at least two pixels
            let range = std::cmp::max(2, self.params.height * self.waveform[x] / peak);
            let start = baseline - range / 2;
            let end = baseline + range / 2;

            for y in start..end {
                let color = if x < until_x {
                    self.params.past_color
                } else {
                    self.params.future_color
                };
                img.put_pixel(x as u32, y, color);
            }
        }
        Ok(())
    }

    fn extract_waveform(
        len: usize,
        source: Box<dyn MediaSource>,
        mime_type: Option<&str>,
        extension: Option<&str>,
    ) -> Result<(Vec<u32>, Time), Error> {
        if len == 0 {
            return Err(Error::ZeroDimensions);
        }

        // Create the media source stream.
        let mss = MediaSourceStream::new(source, Default::default());

        // Create a probe hint using the file's extension. [Optional]
        let mut hint = Hint::new();
        if let Some(ext) = extension {
            hint.with_extension(ext);
        }
        if let Some(mime) = mime_type {
            hint.mime_type(mime);
        }

        // Use the default options for metadata and format readers.
        let meta_opts: MetadataOptions = Default::default();
        let fmt_opts: FormatOptions = Default::default();

        // Probe the media source.
        let probed = symphonia::default::get_probe()
            .format(&hint, mss, &fmt_opts, &meta_opts)
            .map_err(Error::CodecError)?;

        // Get the instantiated format reader.
        let mut format = probed.format;

        // Find the first audio track with a known (decodeable) codec.
        let track = format
            .tracks()
            .iter()
            .find(|t| t.codec_params.codec != CODEC_TYPE_NULL)
            .expect("no supported audio tracks");

        let sample_rate = track.codec_params.sample_rate.unwrap();

        // Use the default options for the decoder.
        let dec_opts: DecoderOptions = Default::default();

        // Create a decoder for the track.
        let mut decoder = symphonia::default::get_codecs().make(&track.codec_params, &dec_opts)?;

        // Store the track identifier, it will be used to filter packets.
        let track_id = track.id;

        let mut current_sample = 0;
        let mut last_pixel_index = 0;
        let mut waveform = vec![0; len];
        let mut current_element = 0.;

        let n_samples = Self::count_samples(&mut format);
        let total_time = n_samples as f64 / sample_rate as f64;

        if n_samples == 0 {
            return Err(Error::EmptyAudioFragment);
        }

        // The decode loop.
        loop {
            // Get the next packet from the media format.
            let packet = match format.next_packet() {
                Ok(packet) => packet,
                Err(SymphoniaError::ResetRequired) => {
                    // The track list has been changed. Re-examine it and create a new set of decoders,
                    // then restart the decode loop. This is an advanced feature and it is not
                    // unreasonable to consider this "the end." As of v0.5.0, the only usage of this is
                    // for chained OGG physical streams.
                    unimplemented!();
                }
                Err(SymphoniaError::IoError(e))
                    if e.kind() == std::io::ErrorKind::UnexpectedEof =>
                {
                    break;
                }
                Err(err) => {
                    // A unrecoverable error occurred, halt decoding.
                    panic!("{:?}", err);
                }
            };

            // Consume any new metadata that has been read since the last packet.
            while !format.metadata().is_latest() {
                // Pop the old head of the metadata queue.
                format.metadata().pop();

                // Consume the new metadata at the head of the metadata queue.
            }

            // If the packet does not belong to the selected track, skip over it.
            if packet.track_id() != track_id {
                continue;
            }

            // Decode the packet into audio samples.
            match decoder.decode(&packet) {
                Ok(decoded) => {
                    // Consume the decoded audio samples (see below).
                    match decoded {
                        AudioBufferRef::F32(buf) => {
                            // XXX: multi chan?
                            for sample in buf.chan(0) {
                                // Do something with `sample`.
                                current_element += sample.abs();

                                let current_pixel_index: usize = len * current_sample / n_samples;
                                if current_pixel_index != last_pixel_index {
                                    waveform[last_pixel_index] = current_element as u32;
                                    current_element = 0.;
                                    last_pixel_index = current_pixel_index;
                                }

                                current_sample += 1;
                            }
                        }
                        _ => {
                            // Repeat for the different sample formats.
                            unimplemented!("sample format")
                        }
                    }
                }
                Err(SymphoniaError::IoError(_)) => {
                    // The packet failed to decode due to an IO error, skip the packet.
                    continue;
                }
                Err(SymphoniaError::DecodeError(_)) => {
                    // The packet failed to decode due to invalid data, skip the packet.
                    continue;
                }
                Err(err) => {
                    // An unrecoverable error occurred, halt decoding.
                    panic!("{}", err);
                }
            }
        }

        Ok((waveform, Time::new(total_time as u64, total_time.fract())))
    }

    fn count_samples(format: &mut Box<dyn FormatReader>) -> usize {
        let mut n_samples = 0;

        // Find the first audio track with a known (decodeable) codec.
        let track = format
            .tracks()
            .iter()
            .find(|t| t.codec_params.codec != CODEC_TYPE_NULL)
            .expect("no supported audio tracks");
        let track_id = track.id;

        // Use the default options for the decoder.
        let dec_opts: DecoderOptions = Default::default();

        // Create a decoder for the track.
        let mut decoder = symphonia::default::get_codecs()
            .make(&track.codec_params, &dec_opts)
            .expect("unsupported codec");

        loop {
            // Get the next packet from the media format.
            let packet = match format.next_packet() {
                Ok(packet) => packet,
                Err(SymphoniaError::ResetRequired) => {
                    // The track list has been changed. Re-examine it and create a new set of decoders,
                    // then restart the decode loop. This is an advanced feature and it is not
                    // unreasonable to consider this "the end." As of v0.5.0, the only usage of this is
                    // for chained OGG physical streams.
                    unimplemented!();
                }
                Err(SymphoniaError::IoError(e))
                    if e.kind() == std::io::ErrorKind::UnexpectedEof =>
                {
                    break;
                }
                Err(err) => {
                    // A unrecoverable error occurred, halt decoding.
                    panic!("{:?}", err);
                }
            };

            // Consume any new metadata that has been read since the last packet.
            while !format.metadata().is_latest() {
                // Pop the old head of the metadata queue.
                format.metadata().pop();

                // Consume the new metadata at the head of the metadata queue.
            }

            // If the packet does not belong to the selected track, skip over it.
            if packet.track_id() != track_id {
                continue;
            }

            // Decode the packet into audio samples.
            match decoder.decode(&packet) {
                Ok(decoded) => {
                    // Consume the decoded audio samples (see below).
                    match decoded {
                        AudioBufferRef::F32(buf) => {
                            n_samples += buf.chan(0).len();
                        }
                        _ => {
                            // Repeat for the different sample formats.
                            unimplemented!("sample format")
                        }
                    }
                }
                Err(SymphoniaError::IoError(_)) => {
                    // The packet failed to decode due to an IO error, skip the packet.
                    continue;
                }
                Err(SymphoniaError::DecodeError(_)) => {
                    // The packet failed to decode due to invalid data, skip the packet.
                    continue;
                }
                Err(err) => {
                    // An unrecoverable error occurred, halt decoding.
                    panic!("{}", err);
                }
            }
        }

        format
            .seek(
                symphonia::core::formats::SeekMode::Accurate,
                symphonia::core::formats::SeekTo::Time {
                    time: Time::new(0, 0.),
                    track_id: Some(track_id),
                },
            )
            .expect("seek back");

        n_samples
    }
}
